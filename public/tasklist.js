// counter
var count = 0;
$(document).ready(function() {
	$("#add").on("click", function() {
		// get user input and priority level
		var txtinput = document.getElementById('item').value;
		var priority = document.getElementById('priority');
		priority = priority.options[priority.selectedIndex].value;
		// create elements and set id
		var txt = document.createElement('p');
		var id = 'task' + count;
		txt.setAttribute('id', id);
		txt.setAttribute('class', 'task');
		txt.innerHTML = txtinput;
		// set color, location of listing
		var color;
		if(priority == 1) {
			$('#med').append(txt);
			color = 'orange';
		} else if(priority == 2) {
			$('#hi').append(txt);
			color = 'red';
		} else {
			$('#low').append(txt);
			color = 'black';
		}
		document.getElementById(id).style.color = color;
		// remove task
		$(".task").on("click", function () {
	        $(this).remove();
	    });
		// reset text box value to empty		
		document.getElementById('item').value = "";
		// increment counter
		count++;					
	});			
});